from nfl.settings import Settings
from pathlib import Path
from nfl.data.make_dataset import run_dataset_pipeline, video_to_frame, zip_folder, download_kaggle_data, prepare_image_annotations


import os

# Set environment variables
os.environ['KAGGLE_USERNAME'] = 'thimmzwiener'
os.environ['KAGGLE_KEY'] = '44ea0ab7d9944ca36ae657271a22050e'

settings = Settings(path_project=Path("."))
from nfl.models.efficientdet2.train import train
# Paramters for Cluster Training. Adapt learning rate when continue with already trained model
settings.nn_parameters.num_gpus = 4
settings.nn_parameters.batch_size = 32
settings.nn_parameters.lr = 1e-5 * 4.
# specify which efficientdet you want to use
settings.nn_parameters.compound_coef = 0     # description="Coefficients of efficientdet"
# load Pretrained COCO parameters
settings.nn_parameters.load_weights = "./nfl/models/efficientdet2/pretrained-efficientdet-d0.pth"  # change path to model/... when you want to use one of our trained models

# fix the backbone and only train rest (recommended in readme)
settings.nn_parameters.head_only = True


# when running the first time, prepare data again (Jonas changed some stuff)
# run_dataset_pipeline(settings)

# train
train(settings)
