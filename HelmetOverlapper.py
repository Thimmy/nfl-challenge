def Diff(players, players2):
    return (list(list(set(players) - set(players2)) + list(set(players2) - set(players))))


OverlapPixelBoundary_Left_EZ = 25  # these need to be fine tuned.....
OverlapPixelBoundary_Top_EZ = 25
OverlapPixelBoundary_Left_SL = 25
OverlapPixelBoundary_Top_SL = 25
result = pd.DataFrame(
    columns=['gameKey', 'PlayID', 'view', 'video', 'frame', 'label', 'left', 'width', 'top', 'height', 'impact',
             'impactType', 'visibility', 'confidence'])
impactNUM = 0;

gameKeys = list(set(tr_labels['gameKey']))
gameKeys = gameKeys[0:1]  # only temp to speed up
for gameKey in gameKeys:  # get a list of game, take one by one...

    playIDs = list(set(tr_labels[(tr_labels["gameKey"] == gameKey)]["playID"]))
    playIDs = playIDs[0:1]
    for playID in playIDs:  # get each play in game, take one by one...

        frames = list(set(tr_labels[((tr_labels["gameKey"] == gameKey) & (tr_labels["playID"] == playID))]["frame"]))
        frames = frames[28:40]
        for frame in frames:  # for each frame...

            players = list(set(tr_labels[((tr_labels["gameKey"] == gameKey) & (tr_labels["playID"] == playID) & (
                    tr_labels["frame"] == frame) & (tr_labels["view"] == "Endzone"))]["label"]))
            players2 = list(set(tr_labels[((tr_labels["gameKey"] == gameKey) & (tr_labels["playID"] == playID) & (
                    tr_labels["frame"] == frame) & (tr_labels["view"] == "Sideline"))]["label"]))

            newPlayerlist = list(set(players).intersection(players2))
            players = newPlayerlist

            for playerOne in players:  # from this list of players, find unique ones, take one by one,

                StatLeft_playerOne_EZ = \
                tr_labels[((tr_labels["gameKey"] == gameKey) & (tr_labels["playID"] == playID) & (
                        tr_labels["frame"] == frame) & (tr_labels["label"] == playerOne) & (
                                   tr_labels["view"] == "Endzone"))]["left"].values[0]

                StatTop_playerOne_EZ = \
                tr_labels[((tr_labels["gameKey"] == gameKey) & (tr_labels["playID"] == playID) & (
                        tr_labels["frame"] == frame) & (tr_labels["label"] == playerOne) & (
                                   tr_labels["view"] == "Endzone"))]["top"].values[0]

                playerList2 = list(set(players) - set([playerOne]))
                # we could potentially speed this up by removing permanently the 'playerOne' from the pool after loop...how?
                for playerTwo in playerList2:  # from the REST of players take one ...

                    StatLeft_playerTwo_EZ = \
                    tr_labels[((tr_labels["gameKey"] == gameKey) & (tr_labels["playID"] == playID) & (
                            tr_labels["frame"] == frame) & (tr_labels["label"] == playerTwo) & (
                                           tr_labels["view"] == "Endzone"))]["left"].values[0]

                    StatTop_playerTwo_EZ = \
                        tr_labels[((tr_labels["gameKey"] == gameKey) & (tr_labels["playID"] == playID) & (
                                tr_labels["frame"] == frame) & (tr_labels["label"] == playerTwo) & (
                                           tr_labels["view"] == "Endzone"))]["top"].values[0]

                    if (
                            StatLeft_playerTwo_EZ - OverlapPixelBoundary_Left_EZ <= StatLeft_playerOne_EZ <= StatLeft_playerTwo_EZ + OverlapPixelBoundary_Left_EZ) \
                            & (
                            StatTop_playerTwo_EZ - OverlapPixelBoundary_Top_EZ <= StatTop_playerOne_EZ <= StatTop_playerTwo_EZ + OverlapPixelBoundary_Top_EZ):

                        # basically, do the above again from the other angle...
                        StatLeft_playerOne_SL = \
                            tr_labels[((tr_labels["gameKey"] == gameKey) & (tr_labels["playID"] == playID) & (
                                    tr_labels["frame"] == frame) & (tr_labels["label"] == playerOne) & (
                                               tr_labels["view"] == "Sideline"))]["left"].values[0]

                        StatTop_playerOne_SL = \
                            tr_labels[((tr_labels["gameKey"] == gameKey) & (tr_labels["playID"] == playID) & (
                                    tr_labels["frame"] == frame) & (tr_labels["label"] == playerOne) & (
                                               tr_labels["view"] == "Sideline"))]["top"].values[0]

                        StatLeft_playerTwo_SL = \
                            tr_labels[((tr_labels["gameKey"] == gameKey) & (tr_labels["playID"] == playID) & (
                                    tr_labels["frame"] == frame) & (tr_labels["label"] == playerTwo) & (
                                               tr_labels["view"] == "Sideline"))]["left"].values[0]

                        StatTop_playerTwo_SL = \
                            tr_labels[((tr_labels["gameKey"] == gameKey) & (tr_labels["playID"] == playID) & (
                                    tr_labels["frame"] == frame) & (tr_labels["label"] == playerTwo) & (
                                               tr_labels["view"] == "Sideline"))]["top"].values[0]

                        if (
                                StatLeft_playerTwo_SL - OverlapPixelBoundary_Left_SL <= StatLeft_playerOne_SL <= StatLeft_playerTwo_SL + OverlapPixelBoundary_Left_SL) \
                                & (
                                StatTop_playerTwo_SL - OverlapPixelBoundary_Top_SL <= StatTop_playerOne_SL <= StatTop_playerTwo_SL + OverlapPixelBoundary_Top_SL):
                            impactNUM = impactNUM + 1
                            video = tr_labels[((tr_labels["gameKey"] == gameKey) & (tr_labels["playID"] == playID) & (
                                    tr_labels["frame"] == frame) & (tr_labels["label"] == playerOne) & (
                                                           tr_labels["view"] == "Endzone"))]["video"].values[0]
                            impact = tr_labels[((tr_labels["gameKey"] == gameKey) & (tr_labels["playID"] == playID) & (
                                    tr_labels["frame"] == frame) & (tr_labels["label"] == playerOne) & (
                                                            tr_labels["view"] == "Endzone"))]["impact"].values[0]
                            impactType = \
                            tr_labels[((tr_labels["gameKey"] == gameKey) & (tr_labels["playID"] == playID) & (
                                    tr_labels["frame"] == frame) & (tr_labels["label"] == playerOne) & (
                                                   tr_labels["view"] == "Endzone"))]["impactType"].values[0]
                            confidence = \
                            tr_labels[((tr_labels["gameKey"] == gameKey) & (tr_labels["playID"] == playID) & (
                                    tr_labels["frame"] == frame) & (tr_labels["label"] == playerOne) & (
                                                   tr_labels["view"] == "Endzone"))]["confidence"].values[0]
                            visibility = \
                            tr_labels[((tr_labels["gameKey"] == gameKey) & (tr_labels["playID"] == playID) & (
                                    tr_labels["frame"] == frame) & (tr_labels["label"] == playerOne) & (
                                                   tr_labels["view"] == "Endzone"))]["visibility"].values[0]
                            height = tr_labels[((tr_labels["gameKey"] == gameKey) & (tr_labels["playID"] == playID) & (
                                    tr_labels["frame"] == frame) & (tr_labels["label"] == playerOne) & (
                                                            tr_labels["view"] == "Endzone"))]["height"].values[0]
                            width = tr_labels[((tr_labels["gameKey"] == gameKey) & (tr_labels["playID"] == playID) & (
                                    tr_labels["frame"] == frame) & (tr_labels["label"] == playerOne) & (
                                                           tr_labels["view"] == "Endzone"))]["width"].values[0]
                            # now, return the information to mark it as impact
                            # return gameKey, playID, view, video, frame, StatLeft_playerOne_EZ, width, StatTop_playerOne_EZ, height, playerOne, impact, impactType, confidence, visiblity, area
                            result.loc[impactNUM] = [gameKey, playID, 'Endzone', video, frame, playerOne,
                                                     StatLeft_playerOne_EZ, width, StatTop_playerOne_EZ, height, impact,
                                                     impactType, visibility, confidence]
