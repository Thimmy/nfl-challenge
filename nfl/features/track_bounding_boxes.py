import pandas as pd
from nfl.settings import Settings
from nfl.data.annotations import left_width_top_height_to_xy_min_max
from nfl.utils import pandas_iterator, calc_iou
import networkx as nx
from tqdm import tqdm


def track_bounding_boxes(bounding_boxes: pd.DataFrame, threshold):
    bounding_boxes["xmin"] = bounding_boxes["left"]
    bounding_boxes["ymin"] = bounding_boxes["top"]
    bounding_boxes["xmax"] = bounding_boxes["left"] + bounding_boxes["width"]
    bounding_boxes["ymax"] = bounding_boxes["top"] + bounding_boxes["height"]
    
    iter_columns = ["gameKey", "playID", "view"]
    outputs = []
    for filters, frames in tqdm(
        pandas_iterator(bounding_boxes, iter_columns), 
        total=len(bounding_boxes[iter_columns].drop_duplicates()),
        desc="Track Helmets"
    ):
        tracking = []
        previous_frame = frames[frames["frame"] == 1]
        helmets = [{"index": x, "from": x, "to": x, "iou": 1} for x in previous_frame.index]
        tracking.append(helmets)
        for frame_id in frames["frame"].unique()[1:]:
            this_frame = frames[frames["frame"] == frame_id]
            iou = (
                pd.DataFrame(
                    calc_iou(
                        this_frame[["xmin", "ymin", "xmax", "ymax"]].values, 
                        previous_frame[["xmin", "ymin", "xmax", "ymax"]].values),
                    index=this_frame.index,
                    columns=previous_frame.index
                )
            )
            idx_max = iou.idxmax(axis=1)
            helmets = [
                {"index": idx, "from": f if value >= threshold else to, "to": to, "iou": value} 
                for idx, f, to, value in zip(iou.index, idx_max, idx_max.index, iou.max(axis=1))]
            tracking.append(helmets)
            previous_frame = this_frame
        connect_bbs = pd.DataFrame([x for y in tracking for x in y]).set_index("index")
        G = nx.from_pandas_edgelist(connect_bbs, 'from', 'to', "iou")
        output = dict(filters)
        output["helmets"] = {f"helmet_{idx}": sorted(list(x)) for idx, x in enumerate(nx.connected_components(G))}
        outputs.append(output)
    return outputs