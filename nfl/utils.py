from pathlib import Path
import pandas as pd
import numpy as np

def get_project_root() -> Path:
    """
    Function to find project path independently of user etc.
    :return:
    """
    return Path(__file__).parent.parent


def pandas_iterator(df, columns):
    unique_combinations = df[columns].drop_duplicates().sort_values(columns)
    for unique_combination in unique_combinations.itertuples(index=False):
        query_list = []
        query_tuples = []
        for key, value in zip(unique_combination._fields, unique_combination):
            query_list.append(f'{key}=="{value}"')
            query_tuples.append((key, value))
            query = ' & '.join(query_list)
        yield query_tuples, df.query(query)
        
        
def calc_iou(bboxes1, bboxes2):
    x11, y11, x12, y12 = np.split(bboxes1, 4, axis=1)
    x21, y21, x22, y22 = np.split(bboxes2, 4, axis=1)
    xA = np.maximum(x11, np.transpose(x21))
    yA = np.maximum(y11, np.transpose(y21))
    xB = np.minimum(x12, np.transpose(x22))
    yB = np.minimum(y12, np.transpose(y22))
    interArea = np.maximum((xB - xA + 1), 0) * np.maximum((yB - yA + 1), 0)
    boxAArea = (x12 - x11 + 1) * (y12 - y11 + 1)
    boxBArea = (x22 - x21 + 1) * (y22 - y21 + 1)
    iou = interArea / (boxAArea + np.transpose(boxBArea) - interArea)
    return iou
