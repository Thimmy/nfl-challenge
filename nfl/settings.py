import pydantic
from pydantic import BaseModel, Field, validator
from typing import List, Dict, Any
from pathlib import Path
import sys
from datetime import datetime

def add_project_path(p, values):
    full_path = values["path_project"] / p
    return full_path

def add_to_path(p):
    sys.path.append(str(p / 'nfl/models/efficientdet2'))
    return p

def create_folder(p, values):
    full_path = add_project_path(p, values)
    (full_path).mkdir(parents=True, exist_ok=True)
    return full_path

def create_run_folder(p, values):
    full_path = values["path_project"] / p / datetime.utcnow().strftime("%Y_%m_%d_%H_%M_%S")
    full_path.mkdir(parents=True, exist_ok=True)
    return full_path


class NNParameters(BaseModel):
    load_weights: str = Field(None, description="Whether to load weights from a checkpoint, set None to initialize, set \'last\' to load last checkpoint")
    num_gpus: int = 0
    batch_size: int = 8
    num_workers: int = Field(4, description="num_workers of dataloader.")
    val_interval: int = Field(1, description='Number of epoches between valing phases')
    head_only: bool = Field(False, description="whether finetunes only the regressor and the classifier, useful in early stage convergence or small/easy dataset")
    debug: bool = Field(False, description="whether visualize the predicted boxes of trainging, the output images will be in test")
    optim: str = Field('adamw', description="select optimizer for training, suggest using \'admaw\' until the very final stage then switch to \'sgd\'")    
    compound_coef: int = Field(0, description="Coefficients of efficientdet") 
    save_interval: int = Field(100, description="'Number of steps between saving'")
    es_min_delta: float = Field(0.0, description="Early stopping\'s parameter: minimum change loss to qualify as an improvement'")
    es_patience:  int = Field(0, description='Early stopping\'s parameter: number of epochs with no improvement after which training will be stopped. Set to 0 to disable this technique.')
    lr: float = Field(1e-4)
    num_epochs: int = Field(20)
    # mean and std in RGB order, actually this part should remain unchanged as long as your dataset is similar to coco.
    mean: List[float] =  [0.485, 0.456, 0.406]
    std: List[float] = [0.229, 0.224, 0.225]

    # this is coco anchors, change it if necessary
    anchors_scales: str =  '[2 ** 0, 2 ** (1.0 / 3.0), 2 ** (2.0 / 3.0)]'
    anchors_ratios: str = '[(1.0, 1.0), (1.4, 0.7), (0.7, 1.4)]'

    # must match your dataset's category_id.
    # category_id is one_indexed,
    # for example, index of 'car' here is 2, while category_id of is 3
    obj_list: List[str] = ['helmet']

    
    
class Settings(BaseModel):
    path_project: Path = Field(".", description="Full path to the project folder. This simplifies running the code from everywhere")
    path_runs: Path = Field("runs")
    path_data_raw: Path = Field("data/raw")
    path_data_processed: Path = Field("data/processed")
    
    path_original_images: Path = Field("data/raw/images")
    path_prepared_images: Path = Field("data/processed/processed_images")
    path_prepared_testing_images: Path = Field("data/processed/processed_testing_images")
    path_annotations: Path = Field("data/processed/annotations")
    
    path_original_test_videos: Path = Field("data/raw/test")
    path_prepared_test_video_images: Path = Field("data/interim/processed_test_video_images")
    path_original_train_videos: Path = Field("data/raw/train")
    path_original_train_player_tracking = Field("data/raw/train_player_tracking.csv")
    path_original_train_labels = Field("data/raw/train_labels.csv")
    path_prepared_train_video_images: Path = Field("data/interim/processed_train_video_images")
    path_original_image_labels: Path = Field("data/raw/image_labels.csv")
        
    path_models: Path = Field("data/models")
    
    image_height: int = Field(1280, description="Height of the sample images.")
    image_width: int = Field(1280, description="Width of the sample images.")
    train_test_split: float = Field(0.1, description="Fraction of images used for validation.")
    nn_parameters: NNParameters = NNParameters()
    
    # Create folder if it does not exists. Needs to be added for each path variable.
    _v_path_project = validator('path_project', allow_reuse=True, always=True)(add_to_path)
    _v_path_data_raw = validator('path_data_raw', allow_reuse=True, always=True)(create_folder)
    _v_path_runs = validator('path_runs', allow_reuse=True, always=True)(create_run_folder)
    _v_path_data_processed = validator('path_data_processed', allow_reuse=True, always=True)(create_folder)
    _v_path_original_images = validator('path_original_images', allow_reuse=True, always=True)(create_folder)
    _v_path_original_image_labels = validator('path_original_image_labels', allow_reuse=True, always=True)(add_project_path)
    _v_path_original_train_player_tracking = validator('path_original_train_player_tracking', allow_reuse=True, always=True)(add_project_path)
    _v_path_path_prepared_images = validator('path_prepared_images', allow_reuse=True, always=True)(create_folder)
    _v_path_prepared_testing_images = validator('path_prepared_testing_images', allow_reuse=True, always=True)(create_folder)
    _v_path_original_test_videos = validator('path_original_test_videos', allow_reuse=True, always=True)(create_folder)
    _v_path_prepared_test_video_images = validator('path_prepared_test_video_images', allow_reuse=True, always=True)(create_folder)
    _v_path_annotations = validator('path_annotations', allow_reuse=True, always=True)(create_folder)
    _v_path_original_train_videos = validator('path_original_train_videos', allow_reuse=True, always=True)(create_folder)
    _v_path_prepared_train_video_images = validator('path_prepared_train_video_images', allow_reuse=True, always=True)(create_folder)
    _v_path_original_train_labels = validator('path_original_train_labels', allow_reuse=True, always=True)(add_project_path)
    
    
    
    
class SubmissionFormat(BaseModel):
    gameKey: str = Field(..., description="the ID code for the game.")
    playID: str = Field(..., description="the ID code for the play.")
    view: str = Field(..., description="the camera orientation.")
    video: str = Field(..., description="the filename of the associated video.")
    frame: str = Field(..., description="the frame number for this play.")
    left: int
    width: int
    top: int
    height: int

