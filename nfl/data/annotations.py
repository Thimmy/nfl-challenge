import numpy as np
import json
import pandas as pd


def _image(row):
    image = {}
    image["height"] = row.height
    image["width"] = row.width
    image["id"] = row.fileid
    image["file_name"] = row.filename
    return image

def _category(row):
    category = {}
    category["supercategory"] = 'None'
    category["id"] = row.categoryid
    category["name"] = row.class_name
    return category

def _annotation(row):
    annotation = {}
    area = (row.xmax -row.xmin)*(row.ymax - row.ymin)
    annotation["segmentation"] = []
    annotation["iscrowd"] = 0
    annotation["area"] = area
    annotation["image_id"] = row.fileid

    annotation["bbox"] = [row.xmin, row.ymin, row.xmax -row.xmin,row.ymax-row.ymin ]

    annotation["category_id"] = row.categoryid
    annotation["id"] = row.annid
    return annotation


def to_coco(data, coco_outpout_path):
    images = []
    categories = []
    annotations = []

    category = {}
    category["supercategory"] = 'none'
    category["id"] = 0
    category["name"] = 'None'
    categories.append(category)

    data['fileid'] = data['filename'].astype('category').cat.codes
    data['categoryid']= pd.Categorical(data['class'],ordered= True).codes
    data['categoryid'] = data['categoryid']+1
    data['annid'] = data.index

    for row in data.itertuples():
        annotations.append(_annotation(row))

    imagedf = data.drop_duplicates(subset=['fileid']).sort_values(by='fileid')
    for row in imagedf.itertuples():
        images.append(_image(row))

    catdf = data.drop_duplicates(subset=['categoryid']).sort_values(by='categoryid')
    catdf = catdf.rename({"class": "class_name"}, axis=1)
    for row in catdf.itertuples():
        categories.append(_category(row))

    data_coco = {}
    data_coco["images"] = images
    data_coco["categories"] = categories
    data_coco["annotations"] = annotations


    json.dump(data_coco, open(coco_outpout_path, "w"), indent=4)


def xy_min_max_to_left_width_top_height(xmin, ymin, xmax, ymax):
    "Transform TF csv coordinates into nfl challenge coordinates"
    left = int(xmin)
    width = int(xmax - xmin)
    top = ymax
    height = ymax - ymin
    return left, width, top, height


def left_width_top_height_to_xy_min_max(left, width, top, height):
    "Transform to coco xmin, ymin, xmax, ymax"
    return left, top, left + width, top + height