# -*- coding: utf-8 -*-
from pathlib import Path


import imgaug as ia
import imgaug.augmenters as iaa
import imageio
from tqdm import tqdm
from nfl.settings import Settings
import pandas as pd
import numpy as np
from nfl.data.annotations import to_coco
import subprocess
import cv2
import shutil
from kaggle.api.kaggle_api_extended import KaggleApi
from sklearn.model_selection import train_test_split 

def download_kaggle_data(settings):
    api = KaggleApi()
    api.authenticate()
    zip_file = settings.path_data_raw / "nfl-impact-detection.zip"
    if not zip_file.exists():
        api.competition_download_files("nfl-impact-detection", path=settings.path_data_raw)
    shutil.unpack_archive(str(zip_file) , str(settings.path_data_raw))


def prepare_image_annotations(annotations_path, settings):
    print("Image annotations.")
    print("\tStart preparing image annotations.")

    annotations = pd.read_csv(annotations_path)

    # clean data. There is one box for frame 0
    annotations = annotations[annotations.frame != 0]


    if "video" in annotations:
        annotations["image"] = annotations["video"].str[:-4] + "_frame_" + annotations["frame"].astype(str) +  ".jpg"
        annotations["label"] = "Helmet"

    annotations["xmin"] = annotations["left"]
    annotations["xmax"] = annotations["left"] + annotations["width"]
    annotations["ymin"] = annotations['top']
    annotations["ymax"] = annotations['top'] + annotations['height']

    annotations = annotations.rename({
        "image": "filename",
        "label": "class"},
        axis=1
    )
    annotations['width'] = settings.image_width
    annotations["height"] = settings.image_height
    annotations["class"] = "helmet"
    annotations = annotations[["filename", "width", "height", "class", "xmin", "ymin", "xmax", "ymax"]]
    print("\tFinished preparing image annotations.")

    return annotations


def prepare_images(annotations, settings: Settings, use_cached=False):
    print("Prepare Images.")
    if not use_cached or not len(list(settings.path_prepared_images.glob("*jpg"))) >= len(annotations["filename"].unique()):
        
        print("\tStart preparing images")
        images_input_paths = {
            # **{x.name: x for x in settings.path_original_images.glob("*")},
            **{x.name: x for x in settings.path_prepared_train_video_images.glob("*")}
        }
        pad_width = int((settings.image_width - 720) / 2)
        seq = iaa.Sequential([iaa.Pad(px=(pad_width, 0, pad_width, 0), keep_size=False),])
        for idx, filenames in enumerate(train_test_split(annotations["filename"].unique(), test_size=settings.train_test_split, random_state=42)):
            if idx == 0:
                annotation_set = "train"
                output_folder = settings.path_prepared_images
                annotations_path = settings.path_annotations / "instances_processed_images.json"
            if idx == 1:
                annotation_set = "test"
                output_folder = settings.path_prepared_testing_images
                annotations_path = settings.path_annotations / "instances_processed_testing_images.json"
                
            print(f"\tPreparing {annotation_set}")
            bbs_augs = []
            for filename in tqdm(filenames):
                try:
                    images = np.expand_dims(imageio.imread(images_input_paths[filename]), axis=0)
                    subset = annotations.loc[annotations["filename"] == filename]
                    bbs = list(subset[["xmin", "ymin", "xmax", "ymax"]].apply(lambda x: ia.BoundingBox(*x), axis=1))
                    images_aug, bbs_aug = seq(images=images, bounding_boxes=bbs)
                    bbs_aug = pd.DataFrame([
                        {
                            "xmin": x.x1,
                            "ymin": x.y1,
                            "xmax": x.x2,
                            "ymax": x.y2
                        }
                        for x in bbs_aug], index=subset.index)
                    bbs_aug = subset[["filename", "width", "height", "class"]].join(bbs_aug)
                    bbs_augs.append(bbs_aug)
                    imageio.imsave(output_folder / filename, np.squeeze(images_aug))
                except KeyError as e:
                    print(filename, images_input_paths[filename], e)
            # Annotations have to be stored in coco with the new image size.
            to_coco(pd.concat(bbs_augs), str(annotations_path))

        print(f"Finished preparing {len(annotations)} images.")
    else:
        print("\tUsing cached version")

        
def video_to_frame(path_input_videos: Path, path_output_images, use_cached=False):
    '''
    Creates single image frames for all the videos in the test folder.
    '''
    # TODO: refactor to make this more robust (Potentially give path as argument
    print("Start transforming videos to frames")
    mp4 = list(path_input_videos.glob('*.mp4'))
    if not use_cached: 
        for test_vid in tqdm(mp4):
            vidcap = cv2.VideoCapture(str(test_vid))
            success, image = vidcap.read()
            count = 1 # has to start at one because the frames are counted from one.
            while success:
                image_path = path_output_images / f"{test_vid.stem}_frame_{count}.jpg"
                cv2.imwrite(str(image_path), image)  # save frame as JPEG file
                success, image = vidcap.read()
                count += 1


def zip_folder(folder, output_filename, use_cached=False):
    output_filename = Path(output_filename)
    print("Zipping folder")
    if not use_cached or output_filename.exists() is False: 
        shutil.make_archive(output_filename, 'zip', folder)
        print("\tFolder ziped")
    else:
        print("\tUse cached zip file.")


def run_dataset_pipeline(settings: Settings, use_cached=False):
    # download_kaggle_data(settings)
    # video_to_frame(settings.path_original_test_videos, settings.path_prepared_test_video_images, use_cached=use_cached)
    # video_to_frame(settings.path_original_train_videos, settings.path_prepared_train_video_images, use_cached=use_cached)
    
    train_labels = prepare_image_annotations(settings.path_original_train_labels, settings)
    # This does not make sense. It is the dataset containing the impacts.
    # image_labels = prepare_image_annotations(settings.path_original_image_labels, settings)
    # annotations = pd.concat([train_labels, image_labels])
    annotations = train_labels
    prepare_images(annotations, settings, use_cached=use_cached)
#     zip_folder(settings.path_data_processed, settings.path_data_processed, use_cached=True)
