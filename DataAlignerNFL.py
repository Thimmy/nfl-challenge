"code that aligns NFL-challenge player sensor and video data"
import bisect

#set paths
paths = {
    "image_path": "images",
    "train_videos": "train",
    "test_videos": "test",
    "image_labels": "image_labels.csv",
    "train_labels": "train_labels.csv",
    "train_player_tracking": "train_player_tracking.csv",
    "test_player_tracking": "test_player_tracking.csv",
    "sample_submissions": "sample_submission.csv"
}
paths = {x: data_path / y for x, y in paths.items()}

#make df video data
trainingLabels = pd.read_csv(paths["train_labels"])

#make df player data
trackingLabels = pd.read_csv(paths["train_player_tracking"])

#problem = player data starts before the video data...
#solution = align at 'ball_snap' -  datapoints, as video data starts about 10 frames before this moment

## what are the columns we want to add?
trainingLabels['x']= 'NaN'
trainingLabels['y']= 'NaN'
trainingLabels['s']= 'NaN'
trainingLabels['a']= 'NaN'
trainingLabels['dis']= 'NaN'
trainingLabels['o']= 'NaN'
trainingLabels['dir']= 'NaN'
trainingLabels['event']= 'NaN'

#loop through each game, playID, player, and video view; and add the player data as new columns
gameKeys = list(set(trainingLabels['gameKey']))
#gameKeys = gameKeys[0:1]  # only temp to test
for gameKey in gameKeys:  # get a list of game, take one by one...

    playIDs = list(set(tr_labels[(trainingLabels["gameKey"] == gameKey)]["playID"]))
    #playIDs = playIDs[0:1] #test
    for playID in playIDs:  # get each play in game, take one by one...

        #this is important: check the player lists from both data sets or you will get an error as sometimes players are marked V00 and H00 in the video, but not in the sensor data
        players = list(set(trainingLabels[((trainingLabels["gameKey"] == gameKey) & (trainingLabels["playID"] == playID) & (trainingLabels["view"] == "Endzone"))]["label"]))
        players2 = list(set(trackingLabels[((trackingLabels["gameKey"] == gameKey) & (trackingLabels["playID"] == playID))]["player"]))
        newPlayerlist = list(set(players).intersection(players2))
        players = newPlayerlist

        for playerOne in players:  # from this list of players, find unique ones, take one by one,

            #then we need to find the loc for the frames for the video data ...
            a = trainingLabels[((trainingLabels["gameKey"] == gameKey) & (trainingLabels["playID"] == playID) & (trainingLabels["label"]==playerOne) & (trainingLabels["view"]== "Endzone"))]
            #what do we want to do in the end? align at 'ball_snap' event! , so let's find it first...
            b = trackingLabels[((trackingLabels["gameKey"] == gameKey) & (trackingLabels["playID"] == playID) & (trackingLabels['event']=='ball_snap') & (trackingLabels["player"]==playerOne))]

            indexCounter = a["frame"].iloc[0:]
            framesAvailable=indexCounter.values[:]
            lastFrame = indexCounter.iloc[-1]

            sensorData = b.index[0]
            lastEntry = b.iloc[[-1]].index[0]

            startFrame = 10
            gapFrame = 6
            x = 0
            secondRun = False

            #find all the impact frames for that player
            impacts = trainingLabels[((trainingLabels["gameKey"] == gameKey) & (trainingLabels["playID"] == playID) & (trainingLabels["label"]==playerOne) & (trainingLabels["view"]== "Endzone")& (trainingLabels["impact"]==1.0))]

            for frameUsed in range(startFrame, lastFrame, gapFrame):
                #need to check if this exists! if it does, great. but often they are missing.
                #if it doesn't we need to skip the sensor datapoint as well and go to the next one!
                x = x + 1


                l = [i for i in impacts['frame'].values]
                lower_bound = frameUsed
                upper_bound = frameUsed + 5
                nums = []
                lower_bound_i = bisect.bisect_left(l, lower_bound)
                upper_bound_i = bisect.bisect_right(l, upper_bound, lo=lower_bound_i)
                nums = l[lower_bound_i:upper_bound_i]

                if len(nums) > 0:

                    for j in range (0,len(nums),1):
                        currentIndexForData = impacts.index[impacts['frame'].values == nums[j]]
                        currentIndexForData = currentIndexForData.values[0]

                        trainingLabels.loc[[currentIndexForData], ['x']] = trackingLabels.loc[sensorData-1+x]['x']
                        trainingLabels.loc[[currentIndexForData], ['y']] = trackingLabels.loc[sensorData-1+x]['y']
                        trainingLabels.loc[[currentIndexForData], ['s']] = trackingLabels.loc[sensorData-1+x]['s']
                        trainingLabels.loc[[currentIndexForData], ['a']] = trackingLabels.loc[sensorData-1+x]['a']
                        trainingLabels.loc[[currentIndexForData], ['dis']] = trackingLabels.loc[sensorData-1+x]['dis']
                        trainingLabels.loc[[currentIndexForData], ['o']] = trackingLabels.loc[sensorData-1+x]['o']
                        trainingLabels.loc[[currentIndexForData], ['dir']] = trackingLabels.loc[sensorData-1+x]['dir']
                        trainingLabels.loc[[currentIndexForData], ['event']] = trackingLabels.loc[sensorData-1+x]['event']


                elif frameUsed in framesAvailable:
                    i, = np.where(framesAvailable == frameUsed)
                    currentIndexForData = indexCounter.index[i][0]

                    #take that loc and add some data! (and iterate every 6 frames)
                    trainingLabels.loc[[currentIndexForData], ['x']] = trackingLabels.loc[sensorData-1+x]['x']
                    trainingLabels.loc[[currentIndexForData], ['y']] = trackingLabels.loc[sensorData-1+x]['y']
                    trainingLabels.loc[[currentIndexForData], ['s']] = trackingLabels.loc[sensorData-1+x]['s']
                    trainingLabels.loc[[currentIndexForData], ['a']] = trackingLabels.loc[sensorData-1+x]['a']
                    trainingLabels.loc[[currentIndexForData], ['dis']] = trackingLabels.loc[sensorData-1+x]['dis']
                    trainingLabels.loc[[currentIndexForData], ['o']] = trackingLabels.loc[sensorData-1+x]['o']
                    trainingLabels.loc[[currentIndexForData], ['dir']] = trackingLabels.loc[sensorData-1+x]['dir']
                    trainingLabels.loc[[currentIndexForData], ['event']] = trackingLabels.loc[sensorData-1+x]['event']
